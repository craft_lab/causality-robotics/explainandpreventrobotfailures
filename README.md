# ExplainAndPreventRobotFailures

This repository contains the code to reproduce the failure explanation, prediction, and prevention methods from the two papers:

[1] M. Diehl and K. Ramirez-Amaro, **"A causal-based approach to explain, predict and prevent failures in robotic tasks,"** in Robotics and Autonomous Systems, 162, 104376, 2023 https://doi.org/10.1016/j.robot.2023.104376

[2] M. Diehl and K. Ramirez-Amaro, **"Why Did I Fail? A Causal-Based Method to Find Explanations for Robot Failures,"** in IEEE Robotics and Automation Letters, vol. 7, no. 4, pp. 8925-8932, Oct. 2022, doi: 10.1109/LRA.2022.3188889.

Please cite *Diehl23* if you use the code for failure prevention and *Diehl22* if you use the code for failure explanation. 

## Installation instructions
### Install R
- Mac: Download and install R from https://cran.rstudio.com/bin/macosx/ 

- Windows: Download and install R from https://cran.r-project.org/bin/windows/base/ 

- Ubuntu (20.04):
```
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9
sudo add-apt-repository 'deb https://cloud.r-project.org/bin/linux/ubuntu focal-cran40/' 
sudo apt update
sudo apt install r-base
```
### Install RStudio
- Mac/Windows: Download and install RStudio from https://posit.co

- Ubuntu: First, download the RStudio .deb file (corresponding to your Ubuntu version) from https://posit.co/download/rstudio-desktop/#download. Then open the terminal and perform the following steps: 
```
$ sudo apt update
$ sudo apt -y install r-base gdebi-core
$ sudo gdebi rstudio-<replace-with-downloaded-version>-amd64.deb
```
### Install required R packages
Finally, open RStudio and perform the following commands in the RStudio console:
```
install.packages("BiocManager")
BiocManager::install("Rgraphviz")
install.packages("rstudioapi")
install.packages("Rcpp")
install.packages("bnlearn") 
install.packages("lattice")
```
Potential requirement for graphviz: before being able to BiocManager::install("Rgraphviz"), it might be required to download graphviz following instructions from https://graphviz.org/download/

### Install Rtools (only required on Windows)
In order to compile .cpp files in RStudio on Windows, the installation of *Rtools* is required. When running source("createFailurePreventionPolicies.R") (or any of the other two R scripts as explained in the **Run the code** section), RStudio will automatically detect that **Compiling C/C++ code for R requires installation of additional build tools**. It will ask you if you want to **install the additional tools now?**. Please confirm this with *Yes*. Then the required installation steps will be conducted automatically.

### Tested versions
- Tested R version: 4.3.0
- Tested RStudio version: 2023.03.0+386 
---
- Tested Mac version: macOS Monterey (Intel Chip)
- Tested Windows version: Windows 10
- Tested Ubuntu version: 20.04
--- 
- Tested bnlearn version: 4.8.1
--- 
Note: other versions will likely also work but without any guarantee!


## Run the code
This repository contains two types of files: 
1. **failureExplanationPrevention.cpp** contains the main implementation of the methods from *Diehl22* and *Diehl23*
2. The R files (e.g., **FailureExplanation_DropSphereExperiment.R**) contain functionality to process the datasets and reproduce the examples from the papers (Diehl22-TableI and Diehl23-Table5). 

### Start and prepare RStudio
1. Clone this repository to your computer
2. Open R studio
3. In the Files tab of the Output pane (https://docs.posit.co/ide/user/ide/guide/ui/files.html): navigate to the cloned repository and set it as working directory
### Reproduce the examples from the papers
1. To reproduce the failure prevention example from the paper *Diehl23*, run the following command in the R console:
```
source("createFailurePreventionPolicies.R")
```
2. To reproduce the failure explanation of the DropSphere experiment from the paper *Diehl22*, run the following command in the R console: 
```
source("FailureExplanation_DropSphereExperiment.R")
```
3. To reproduce the failure explanation of the CubeStacking experiment from the paper *Diehl22*, run the following command in the R console: 
```
source("FailureExplanation_CubeStacking.R")
```
**When running these examples, statistics about the data (e.g., failure distribution) and the failure prevention/explanation examples from the papers (Diehl22-TableI and Diehl23-Table5) will be printed inside the R console. Furthermore, plots of the variable distribution and the obtained causal Bayesian-Network will be displayed in the Plot tab of the Output pane.**

