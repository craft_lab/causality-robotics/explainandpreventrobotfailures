/*! \file
 *
 * \author Maximilian Diehl
 *
 * \date 27.05.2023
 *
 *
 * #### License
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

// // Cpp implementation of failure prevention policy creation


// // Embedd R code inside Cpp
#include "Rcpp.h" 
using namespace Rcpp;

#include <map>
#include <algorithm>
#include <iostream>
#include <string>
#include <iterator>
#include <vector>
#include <list>
#include <stdexcept>
#include <chrono>



struct Interval {
	double lowerBound, upperBound;
	std::string intervalString;
};

/* compare two intervals based on lowerBound */
bool compareInterval(Interval i1, Interval i2) {
	return (i1.lowerBound < i2.lowerBound);
}


struct SuccessLookupTable {
	int index; // index corresponding to the data row 

	std::vector<int> intervalCategorical; // sorted interval index  
	std::vector<std::string> intervalString; // corresponding interval string

	double success; // action success chance given the current intervals
};

class CategoryValues {

public: 
	int dimension; 
	Rcpp::List dimensions; // number of unique intervals per variable
	//std::map<std::string, std::vector<int>> dimensions;
	std::map<std::string, std::vector<Interval>> intervals;
	std::vector<std::string> relParams;



	/* retrieve index of a given category interval with respect to the previously sorted list of all intervals
	of that specific category */
	int getIntervalIndex(std::string category, std::string interval) {

		for (int i = 0; i < intervals[category].size(); i++) {
			if (intervals[category][i].intervalString == interval) {
				return i;
			}
		}
		std::cout << "Index was not found" << std::endl;
		return -1;
	}

	/* retrieve all map values (which correspond to the category values) */
	std::vector<std::string> getCategoryNames() {
		std::vector<std::string> catNames;
		for (auto const& imap : intervals) {
			catNames.push_back(imap.first);
		}
		return catNames;
	}

};

struct SearchGraph {
	Rcpp::NumericMatrix searchGraphMatrix;
	std::map<int, std::vector<int>> searchGraphList; 
};

struct BNParameters {
	Rcpp::DataFrame arcs; 
	Rcpp::String outcome;
	Rcpp::StringVector blackList;
	int add;
};


/* 
- Transforms an interval string into struct type Interval
- Can deal with intervals of type (lowerBound,upperBound] such as xOff or binary variables such as onTop 
*/
Interval stringToInterval(const std::string& stringInterval) {

	// this function can currently deal with intervals of type (lowerBound,upperBound] such as xOff or binary variables such as onTop

	// first find out the type of variables 
	if (((stringInterval[0]=='(' | stringInterval[0]=='[')) & (stringInterval.find(',') != std::string::npos)) {
		// remove the brakets 
		std::string stringIntervalCopy = stringInterval;
		stringIntervalCopy.erase(0,1);
		stringIntervalCopy.erase(stringIntervalCopy.size() - 1);

		// split the string based on the ","
		Interval intervalOut = {std::stod(stringIntervalCopy.substr(0, stringIntervalCopy.find(","))),
							 	std::stod(stringIntervalCopy.substr(stringIntervalCopy.find(",")+1)),
							 	stringInterval}; 

		return intervalOut;



	} else if (stringInterval == "0" | stringInterval == "1" ) {
		Interval intervalOut = {std::stod(stringInterval),
								std::stod(stringInterval),
								stringInterval};

		return intervalOut;
		
	} else {
		throw std::invalid_argument( "received unsupported variable interval value" );
	}
}

/* transforms a vector of string intervals into a vector of struct Intervals */
std::vector<Interval> stringsToIntervals(std::vector<std::string> stringIntervals) {

	std::vector<Interval> intervals;
	for(int i=0; i<stringIntervals.size(); i++) {
		intervals.push_back(stringToInterval(stringIntervals[i]));
	}

	return intervals;
}


/* convert a DataFrame into encoded vector*/
std::tuple<std::vector<std::string>, std::vector<std::string>> encodeDataFrame(const Rcpp::DataFrame& df, CategoryValues& categoryValues, const std::string outcome) {

	//std::vector<std::string> catNames = categoryValues.getCategoryNames();

	std::vector<std::string> dfEncoded;
	std::vector<std::string> dfEncodedOutcome; 
	if (df.containsElementNamed(outcome.c_str())) {

		// create map of all data values so we don't need to extract the values several times
		std::map<std::string, std::vector<std::string>> dataFrameVector;
		// create map of all unique category values
		std::map<std::string, std::vector<std::string>> categoriesVector; 
		// get the outcome variable values
		std::vector<std::string> outcomeVal = df[outcome];

		for (auto const& cat : categoryValues.relParams) {
	 		// access each column of the dataFrame
	 		std::vector<std::string> dfPerCat = df[cat];
	 		dataFrameVector.insert({cat, dfPerCat});
		}


		// loop through the dataframe rows and encode each line
		for (int i = 0; i < df.nrows(); i++) {
			std::string codeWord = "";
			for (std::size_t c = 0; c < categoryValues.relParams.size(); c++) {
				// get the index of each value 
				std::string cat = categoryValues.relParams[c];

				codeWord = codeWord + std::to_string(categoryValues.getIntervalIndex(cat, dataFrameVector[cat][i])); 
			}
			// push back the codeWord without outcome
			dfEncoded.push_back(codeWord);

			// push back the action success outcome
			if (outcomeVal[i] == "1") {
				codeWord = codeWord + std::to_string(1); // success
			} else {
				codeWord = codeWord + std::to_string(0); // failure
			}

			// push back the codeWord with action outcome
			dfEncodedOutcome.push_back(codeWord);
		}
	}

	//sort the vectors --> makes it easier to count how often each row occurs
	sort(dfEncoded.begin(), dfEncoded.end());
	sort(dfEncodedOutcome.begin(), dfEncodedOutcome.end()); 


	return {dfEncoded, dfEncodedOutcome};

}


/* Recursive generation of all possible category combination of all the possible variables */
Rcpp::StringVector recursiveList(std::map<std::string, std::vector<Interval>> catValuesList, std::vector<std::string> catNames, int level, int stop, std::string existingName, std::vector<int> existingCategoryCombinations, std::vector<std::vector<int>>& categoriesGrid) {


	Rcpp::StringVector namesList;

	// get the next-level variable
	std::vector<Interval> nextLevelCatVec = catValuesList.at(catNames[level]);


	for (int i=0; i < nextLevelCatVec.size(); i++) { // loop through variables


		// generate name for the current variable
		std::string name = catNames[level] + "__" + nextLevelCatVec[i].intervalString;


		// set the current element (level) to the current category
		existingCategoryCombinations[level] = i;

		// connect it with the variable from previous level
		name = existingName + "__" + name;

		if (level < stop) {
			// recursive list is a vector
			Rcpp::StringVector element = recursiveList(catValuesList, catNames, level+1, stop, name, existingCategoryCombinations, categoriesGrid);
			for (auto const& el : element) {
				namesList.push_back(el);
			}

		} else {
			namesList.push_back(name);
			categoriesGrid.push_back(existingCategoryCombinations);		
		}
	}

	return namesList;

}


/* Get all relevant perameters */
std::vector<std::string> getRelevantVariables(const BNParameters& bnParameters) {

	// initialize return variable
	std::vector<std::string> relParams;

	// process arcs of the bayesian network
	const Rcpp::StringVector arcsFrom = bnParameters.arcs["from"];
	const Rcpp::StringVector arcsTo = bnParameters.arcs["to"];

	// append non-blacklisted nodes that point towards 'outcome'
	for(int i=0; i < arcsTo.size(); i++) {
		if (arcsTo[i] == bnParameters.outcome && (std::count(bnParameters.blackList.begin(), bnParameters.blackList.end(), arcsFrom[i]) == 0)) {
			relParams.push_back(Rcpp::as<std::string>(arcsFrom[i])); // relParams size is not known beforehand
		}
	}

	return relParams;
}

// [[Rcpp::export]]
std::vector<std::string> getRelevantVariablesCpp(const Rcpp::DataFrame arcs, const Rcpp::String outcome, const Rcpp::StringVector blackList) {

	BNParameters bnParameters{arcs, outcome, blackList};

	return getRelevantVariables(bnParameters);

}


CategoryValues getSortedIntervals(const std::vector<std::string> relParams, const Rcpp::DataFrame& df) {


	std::map<std::string, std::vector<Interval>> catValuesList{};
	int dimension = 1; // dimension is the multiplication of all variable intervals
	Rcpp::List dimensions = List::create(); // keep track of each individual number of variable intervals

	std::cout << "Relevant variabels and their intervals:" << std::endl;
	for(std::string cat : relParams) {

		// get list with unique category values
		std::vector<std::string> categories = df[cat];
		std::set<std::string> const uniques(categories.begin(), categories.end());
    	categories.assign(uniques.begin(), uniques.end());
    	
    	// transform the strings into interval structs
    	std::vector<Interval> intervals = stringsToIntervals(categories);

    	// sort the categories
    	sort(intervals.begin(), intervals.end(), compareInterval);

    	// print the Bounds
    	std::cout << cat << std::endl;
    	for (auto x : intervals) 
     		std::cout << "[" << x.lowerBound << ", " << x.upperBound << "] " << std::endl; 

    	// append the intervals to the cat_values_list dictionary
    	catValuesList.insert(std::pair<std::string, std::vector<Interval>>(cat, intervals));

    	dimensions.push_back(intervals.size());
    	dimension = dimension*intervals.size(); 		
	}

	CategoryValues categoryValues{dimension, dimensions, catValuesList, relParams};

	return categoryValues;
}



/* Create SearchGraph which is used for the search procedure to find the closest success variable parametrization*/
SearchGraph createSearchGraph(const std::vector<std::string> relParams, 
							  CategoryValues& categoryValues,
							  const BNParameters bnParameters,
							  const std::vector<std::vector<int>> categoriesGrid,
							  const Rcpp::StringVector searchGraphInformation) { 
	

	// initialize the search graph
	Rcpp::NumericMatrix searchGraphMatrix(categoryValues.dimension, categoryValues.dimension);
	rownames(searchGraphMatrix) = searchGraphInformation;
	colnames(searchGraphMatrix) = searchGraphInformation;

	// initialize second output argument
	std::map<int, std::vector<int>>  searchGraphList;

	// generate transition matrix with all possible 1-interval-change transitions
	// Idea: loop through grid vectors --> example vector [1, 2, 3, 1, 1]
	// Then loop throuhgh that vector and either add or substract +-1; Then check if this is within bounds and in case it is save the transition
	// E.g. [1, 2, 3, 1, 1] --> [2, 2, 3, 1, 1]; [1, 2, 3, 1, 1] --> [1, 3, 3, 1, 1]; ...
	for (int r = 0;  r<categoriesGrid.size(); r++) {
		std::vector<int> indicesSucc;
		
		for (int c = 0;  c<categoriesGrid[r].size(); c++) {

			// +	
		  	if (categoriesGrid[r][c]+1 < int(categoryValues.dimensions[c])) {
			    std::vector<int> successor = categoriesGrid[r];
			    successor[c] = successor[c] + 1; 
			    int indSucc = -1; // if indSucc will not be updated the program breaks
			    
			    // get the successor state
			    for (int start=0; start < categoriesGrid.size(); start++) {
			    	if (successor == categoriesGrid[start]) {
			    		indSucc = start;
			    		indicesSucc.push_back(start);
			        	break;
			      	}
			    }

			    if (indSucc==-1) {
			    	std::cout << "Warning: the graph might be wrong!" << std::endl;
			    	std::cout << successor[0] << "-" << successor[1] << std::endl;
			    } else {
			    	searchGraphMatrix(r,indSucc) = 1;
			    }
			}
			// - 
			if (categoriesGrid[r][c]-1 >= 0) {
			    std::vector<int> successor = categoriesGrid[r];
			    successor[c] = successor[c] - 1; 
			    int indSucc = -1;
			    
			    // get the successor state
			    for (int start=0; start < categoriesGrid.size(); start++) {
			    	if (successor == categoriesGrid[start]) {
			        	indSucc = start;
			        	indicesSucc.push_back(start);
			        	break;
			      	}
			    }

			    if (indSucc==-1) {
			    	std::cout << "Warning: the graph might be wrong!" << std::endl;
					std::cout << successor[0] << "-" << successor[1] << std::endl;			    
				} else {
			    	searchGraphMatrix(r,indSucc) = 1;
			    }
			}		  
		}

		// sort the vector before insertion 
		sort(indicesSucc.begin(), indicesSucc.end());

		searchGraphList.insert({r, indicesSucc});
	}

	SearchGraph sg{searchGraphMatrix, searchGraphList};
	return sg;
}


/* Wrapper function to call createSearchGraph() */
// [[Rcpp::export]]
Rcpp::NumericMatrix createSearchGraphCpp(const Rcpp::DataFrame df, 
									 	 const Rcpp::DataFrame arcs, 
									   	 const Rcpp::String outcome,
									   	 const Rcpp::StringVector blackList) {


	// get relevant parameters
	BNParameters bnParameters{arcs, outcome, blackList};
	std::vector<std::string> relParams = getRelevantVariables(bnParameters);

	// get unique category values
	CategoryValues categoryValues = getSortedIntervals(relParams, df);

	// obtain list of state encodings (similar to the row/col names of the transition matrix)
	std::vector<int> initialCategoryVector(relParams.size(), 0);
	std::vector<std::vector<int>> categoriesGrid;
	Rcpp::StringVector searchGraphInformation = recursiveList(categoryValues.intervals, relParams, 0, relParams.size()-1, "", initialCategoryVector, categoriesGrid);

	// create search graph
	SearchGraph sg = createSearchGraph(relParams, categoryValues, bnParameters, categoriesGrid, searchGraphInformation);

	return sg.searchGraphMatrix;

}

std::string intToString(const std::vector<int> intVec) {
	std::stringstream ss;
	copy(intVec.begin(), intVec.end(), std::ostream_iterator<int>(ss, ""));
	return ss.str();

}

// [[Rcpp::export]]
void getSuccessChanceCpp(const Rcpp::DataFrame df, 
						 const Rcpp::DataFrame arcs, 
						 const Rcpp::String outcome,
						const Rcpp::StringVector blackList, const std::string codeWord) {

	BNParameters bnParameters{arcs, outcome, blackList};
	std::vector<std::string> relParams = getRelevantVariables(bnParameters);


	CategoryValues categoryValues = getSortedIntervals(relParams, df);


	std::vector<int> initialCategoryVector(relParams.size(), 0);
	std::vector<std::vector<int>> categoriesGrid;
	recursiveList(categoryValues.intervals, relParams, 0, relParams.size()-1, "", initialCategoryVector, categoriesGrid);

	// encoded dataframe
	std::tuple<std::vector<std::string>, std::vector<std::string>> dfEncoded = encodeDataFrame(df, categoryValues, bnParameters.outcome);

	// check how often this variable combination exists in encoded dataFrame
	int nn = std::count(std::get<0>(dfEncoded).begin(), std::get<0>(dfEncoded).end(), codeWord);
	
	// check how often each variable combination is succesfull
	int ns = std::count(std::get<1>(dfEncoded).begin(), std::get<1>(dfEncoded).end(), codeWord+"1");


}

// [[Rcpp::export]]
void testEncodeDataFrameCpp(const Rcpp::DataFrame df, 
									 	 const Rcpp::DataFrame arcs, 
									   	 const Rcpp::String outcome,
									   	 const Rcpp::StringVector blackList,
									   	 const int testIndex) {

	BNParameters bnParameters{arcs, outcome, blackList};
	std::vector<std::string> relParams = getRelevantVariables(bnParameters);


	CategoryValues categoryValues = getSortedIntervals(relParams, df);


	std::vector<int> initialCategoryVector(relParams.size(), 0);
	std::vector<std::vector<int>> categoriesGrid;
	recursiveList(categoryValues.intervals, relParams, 0, relParams.size()-1, "", initialCategoryVector, categoriesGrid);

	std::map<std::string, std::vector<std::string>> dataFrameVector;
	for (auto const& cat : categoryValues.relParams) {
	 	// access each column of the dataFrame
	 	std::vector<std::string> dfPerCat = df[cat];
	 	dataFrameVector.insert({cat, dfPerCat});
	}

	// encoded dataframe
	std::tuple<std::vector<std::string>, std::vector<std::string>> dfEncoded = encodeDataFrame(df, categoryValues, bnParameters.outcome);

}




std::tuple<std::vector<SuccessLookupTable>, Rcpp::DataFrame> createSuccessMatrix(CategoryValues& categoryValues,
																				 const Rcpp::DataFrame& df, 
																				 const BNParameters& bnParameters,
																				 std::vector<std::vector<int>> categoriesGrid) {


	// matrix that captures the action success per specific variable parametrization
	std::vector<double> actualSuccess(categoriesGrid.size(), 0);

	// transpose the categoriesGris so we can add it to the SuccessDf
	std::vector<std::vector<int>> categoriesGridTranspose(categoryValues.relParams.size(), std::vector<int>(categoriesGrid.size()));

	// encoded dataframe
	std::tuple<std::vector<std::string>, std::vector<std::string>> dfEncoded = encodeDataFrame(df, categoryValues, bnParameters.outcome);

	// create vector of SuccessLookupTables
	std::vector<SuccessLookupTable> successLookupTable;

	//loop through the grid and calculate the action success for each variable parametrization
	// initialize two index variables
	int dfCounter = 0;
	int gridCounter = 0;

	while (gridCounter<categoriesGrid.size()) {
		// generate the codeword
		std::string codeWord = intToString(categoriesGrid[gridCounter]);

		std::vector<std::string> intervalStrings;
 
		for (int c = 0; c < categoryValues.relParams.size(); c++) { 

			// replace respective value in categoriesGrid transpose
			categoriesGridTranspose[c][gridCounter] = categoriesGrid[gridCounter][c];

			intervalStrings.push_back(categoryValues.intervals[categoryValues.relParams[c]][categoriesGrid[gridCounter][c]].intervalString);
		}

		int nn_counter = 0;
		int ns_counter = 0;

		while (codeWord==std::get<0>(dfEncoded)[dfCounter]) {
			nn_counter +=1;
			if (codeWord+"1"==std::get<1>(dfEncoded)[dfCounter]) {
				ns_counter +=1;
			}
			dfCounter+=1;
		}

		// update success matrix and create new SuccessLookupTable element
		if (double(nn_counter)==0) {
			actualSuccess[gridCounter]=0;
			SuccessLookupTable slt{gridCounter, categoriesGrid[gridCounter], intervalStrings, 0};
			successLookupTable.push_back(slt);
		} else {
			actualSuccess[gridCounter]= double(ns_counter)/double(nn_counter);
			SuccessLookupTable slt{gridCounter, categoriesGrid[gridCounter], intervalStrings, double(ns_counter)/double(nn_counter)};
			successLookupTable.push_back(slt);
		}

		gridCounter += 1;

	}
	
	// // create dataFrame with the grid and the respective success
	Rcpp::DataFrame actualSuccessDf;
	for (int i = 0; i < categoriesGridTranspose.size(); i++) {
		actualSuccessDf.push_back(categoriesGridTranspose[i], categoryValues.relParams[i]);  
	}
	// add the success chance
	actualSuccessDf.push_back(actualSuccess, bnParameters.outcome);  



	return {successLookupTable, actualSuccessDf};
}


/* Wrapper function to call createSuccessMatrix() */
// [[Rcpp::export]]
Rcpp::DataFrame createSuccessMatrixCpp(const Rcpp::DataFrame df, 
									   const Rcpp::DataFrame arcs, 
									   const Rcpp::String outcome,
									   const Rcpp::StringVector blackList) {


	BNParameters bnParameters{arcs, outcome, blackList};
	std::vector<std::string> relParams = getRelevantVariables(bnParameters);

	// get unique category values
	CategoryValues categoryValues = getSortedIntervals(relParams, df);


	// obtain categoriesGrid from recursiveList function 
	std::vector<int> initialCategoryVector(relParams.size(), 0);
	std::vector<std::vector<int>> categoriesGrid;
	recursiveList(categoryValues.intervals, relParams, 0, relParams.size()-1, "", initialCategoryVector, categoriesGrid);


	std::tuple<std::vector<SuccessLookupTable>, Rcpp::DataFrame> actualSuccessDf = createSuccessMatrix(categoryValues, df, bnParameters, categoriesGrid);


	return std::get<1>(actualSuccessDf);
}

// [[Rcpp::export]]
void encodeDataFrameCpp(const Rcpp::DataFrame df, 
						const Rcpp::DataFrame arcs, 
						const Rcpp::String outcome,
						const Rcpp::StringVector blackList) {


	BNParameters bnParameters{arcs, outcome, blackList};
	std::vector<std::string> relParams = getRelevantVariables(bnParameters);

	// get unique category values
	CategoryValues categoryValues = getSortedIntervals(relParams, df);

	// obtain categoriesGrid from recursiveList function 
	std::vector<int> initialCategoryVector(relParams.size(), 0);
	std::vector<std::vector<int>> categoriesGrid;
	recursiveList(categoryValues.intervals, relParams, 0, relParams.size()-1, "", initialCategoryVector, categoriesGrid);


	// matrix that captures the action success per specific variable parametrization
	std::vector<double> actualSuccess;

	
	// transpose the categoriesGris so we can add it to the SuccessDf
	std::vector<std::vector<int>> categoriesGridTranspose(relParams.size(), std::vector<int>(categoriesGrid.size()));

	// encoded dataframe
	std::tuple<std::vector<std::string>, std::vector<std::string>> dfEncoded = encodeDataFrame(df, categoryValues, bnParameters.outcome);

}

Rcpp::List bfs(std::map<int, std::vector<int>> searchGraph,
			   const int currentState,
			   const Rcpp::StringVector rownames,
			   const std::vector<SuccessLookupTable> successLookupTable,
			   const double epsilon,
			   const int enable_debug) {


	std::list<int> queue;
	queue.push_back(currentState);

	std::vector<int> visited(searchGraph.size(), 0);

	visited[currentState] = 1;

	std::vector<int> distances(searchGraph.size(), -1);

	distances[currentState] = 0;

	int winner = -1;

	while (queue.size() > 0) {
		int node = queue.front();
		queue.pop_front();

		// loop through the successor list of current state
		std::vector<int> successors = searchGraph[node];

		for (auto const& sc : successors) {
			if (visited[sc]==0) {
				visited[sc] = 1;
				distances[sc] = distances[node] + 1;
				queue.push_back(sc);

				if (enable_debug) {
					std::cout << sc << std::endl;
					std::cout << successLookupTable[sc].success << std::endl;
				}

				// goal check
				if (successLookupTable[sc].success >= epsilon) {
					winner = sc;
					Rcpp::List searched = List::create(distances, winner, successLookupTable[winner].success);
					return searched;
				}

				if (enable_debug) {
					std::cout << rownames[sc] << std::endl;
				}
			}
		}
	}

	std::cout << "Cannot find any successfull state! " << std::endl;
	Rcpp::List searched = List::create(distances, currentState, successLookupTable[currentState].success);
	return searched;
}


// [[Rcpp::export]]
Rcpp::DataFrame generateFailurePolicyCpp(const Rcpp::DataFrame df,
									  	 const Rcpp::DataFrame arcs, 
									  	 const Rcpp::String outcome, 
									  	 const Rcpp::StringVector blackList,
									  	 const double epsilon,
									  	 const int enable_debug) {

	// get relevant parameters
	BNParameters bnParameters{arcs, outcome, blackList};
	std::vector<std::string> relParams = getRelevantVariables(bnParameters);

	// get unique category values
	CategoryValues categoryValues = getSortedIntervals(relParams, df);

	// obtain list of state encodings (similar to the row/col names of the transition matrix)
	std::vector<int> initialCategoryVector(relParams.size(), 0);
	std::vector<std::vector<int>> categoriesGrid;
	Rcpp::StringVector searchGraphInformation = recursiveList(categoryValues.intervals, relParams, 0, relParams.size()-1, "", initialCategoryVector, categoriesGrid);

	// get success matrix 
	std::tuple<std::vector<SuccessLookupTable>, Rcpp::DataFrame> actualSuccessTuple = createSuccessMatrix(categoryValues, df, bnParameters, categoriesGrid);
	//Rcpp::DataFrame failurePreventionPolicy = std::get<1>(actualSuccessTuple);
	std::vector<SuccessLookupTable> successLookupTable = std::get<0>(actualSuccessTuple);


	// create search graph
	SearchGraph sg = createSearchGraph(relParams, categoryValues, bnParameters, categoriesGrid, searchGraphInformation);
	Rcpp::StringVector stateNames = rownames(sg.searchGraphMatrix);


	// to fill the outcome data frame we need to transpose the vectors (for the input vector we take the already available output from createSuccessMatrix())
	std::vector<std::vector<std::string>> gridCategories(relParams.size()*2, std::vector<std::string>(categoriesGrid.size()));
	std::vector<double> currentSuccess(successLookupTable.size(), 0);
	std::vector<double> preventionSuccess(successLookupTable.size(), 0);
	std::vector<int> closestSuccessIndVec(successLookupTable.size(), 0);

	std::cout << "Calculate closest success state for all possible state combinations:" << std::endl;
	for (int r = 0; r < successLookupTable.size(); r++) {

		if (enable_debug) {
			std::cout << "Current index: " << r << std::endl;
			std::cout << "Current State: " << stateNames[r] << std::endl;
			std::cout << "Current Success Chance: " << successLookupTable[r].success << std::endl; 
		} else {
			if (r % 1000 == 0) {
				std::cout << std::to_string(r) << "/" << std::to_string(successLookupTable.size()) << std::endl;
			} 
			if (r == successLookupTable.size()-1) {
				std::cout << std::to_string(successLookupTable.size()) << "/" << std::to_string(successLookupTable.size()) << std::endl;
			}
		}
		
		for (int c = 0; c < relParams.size(); c++) {
			gridCategories[c][r] = successLookupTable[r].intervalString[c];
		}
		currentSuccess[r] = successLookupTable[r].success;

		// if current success is below epsilon
		if (successLookupTable[r].success < epsilon) {
			// get failure explanation
			Rcpp::List explanation = bfs(sg.searchGraphList, r, searchGraphInformation, successLookupTable, epsilon, enable_debug);
			int closestSuccessInd = explanation[1];
			double closestSuccessChance = explanation[2];
			closestSuccessIndVec[r] = closestSuccessInd+1; // add 1 because data frames in R are starting from 1
			preventionSuccess[r] = closestSuccessChance;

			for (int c = 0; c < relParams.size(); c++) {
				gridCategories[c+relParams.size()][r] = successLookupTable[closestSuccessInd].intervalString[c];
			}	
			if (enable_debug) {
				std::cout << "Closest Success State: " << stateNames[closestSuccessInd] << std::endl;
				std::cout << "Current Success Chance: " << std::to_string(closestSuccessChance) << std::endl; 
				std::cout << "----------------" << std::endl;
			}

		}
		else {
			closestSuccessIndVec[r] = r+1; // add 1 because data frames in R are starting from 1
			preventionSuccess[r] = successLookupTable[r].success;
			for (int c = 0; c < relParams.size(); c++) {
				gridCategories[c+relParams.size()][r] = successLookupTable[r].intervalString[c];
			}
		}
	}

	// assembly outcome data frame
	Rcpp::DataFrame failurePreventionPolicy;
	for (int c = 0; c < relParams.size(); c++)
		failurePreventionPolicy.push_back(gridCategories[c], relParams[c]);
	failurePreventionPolicy.push_back(currentSuccess, "currentSuccChance");
	failurePreventionPolicy.push_back(closestSuccessIndVec, "closestSuccInt");
	for (int c = 0; c < relParams.size(); c++)
		failurePreventionPolicy.push_back(gridCategories[c+relParams.size()], relParams[c]+"_success");
	failurePreventionPolicy.push_back(preventionSuccess, "closestSuccChance");
	return failurePreventionPolicy;

}




