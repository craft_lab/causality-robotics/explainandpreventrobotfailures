####################################################
# Author: Maximilian Diehl
# Date: 27.05.2023
#
# This code enables users to reproduce the StackCube 
# experiment from the paper: 
# M. Diehl and K. Ramirez-Amaro,
# "Why Did I Fail? A Causal-Based Method to 
# Find Explanations for Robot Failures,"
# in IEEE Robotics and Automation Letters, 2022
# doi: 10.1109/LRA.2022.3188889.
#
# Run by pasting following command in R console: 
# source("FailureExplanation_CubeStacking.R")
#
# License
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
####################################################


library(bnlearn)
library(lattice)


## Set optional parameters:
enable_print = 1 # if enabled, script will print information such as statistics of the data
enable_plots = 1 # if enabled, script will plot information such as the distribution of the variables
enable_debug = 0 # if enabled, additional information regarding the closest success search procedure will be printed 

###################################################
setClass(Class="bn_custom",
         representation(
           graph="bn",
           fitted="bn.fit",
           plot="graph"
         ))


###### discretize function ###### 
disc <- function(df, breaks) {
  
  df_new <- data.frame(sapply(df$Outcome, as.factor), 
                       sapply(bnlearn:::discretize(data.frame(df$dropOff), breaks=breaks[1]),as.factor),
                       sapply(bnlearn:::discretize(data.frame(df$xOff), breaks=breaks[2]),as.factor),
                       sapply(bnlearn:::discretize(data.frame(df$yOff), breaks=breaks[3]),as.factor),
                       sapply(df$color_Cube_up, as.factor),
                       sapply(df$color_Cube_down, as.factor))
  
  colnames(df_new) <- c("onTop", "dropOff", "xOff", "yOff", "color_up", "color_down")
  
  return(df_new)
}

###### Learn the causal graph ######
learnGraph <- function(df, algo, wl) {
  
  # convert all character rows into factor
  df[sapply(df, is.character)] <- lapply(df[sapply(df, is.character)], 
                                         as.factor)
  
  
  if (algo == "gs") {
    graph = gs(df, whitelist=wl) 
  }
  else if (algo =="pc") {
    graph = pc.stable(df, whitelist=wl)
  }
  else {
    print("You have chosen an unknown BN learning algorithm.")
  }
  
  # plot the graph
  par(mfrow = c(1, 1))
  p <- graphviz.plot(graph)
  graph::nodeRenderInfo(p) <- list(fontsize=40)
  Rgraphviz::renderGraph(p)
  
  # fit a probabiltiy table
  fitted = bn.fit(graph, data = df) # maximum likelihood
  #fitted = bn.fit(graph, data = df, method="bayes") 
  
  return(new("bn_custom", graph=graph, fitted=fitted, plot=p))
}
###################################################

###################################################
if (enable_print) {
  print(paste("Current working directory: ", getwd()))
}

# compile the cpp file which contains main failure prevention and explanation functionality
Rcpp::sourceCpp("failureExplanationPrevention.cpp") 

file = "Data/Stack1Cube_SimulationData_RAL.csv"
df_numeric <- read.csv(file=file, header=T,sep=",")


# plot variable statistics
if (enable_plots) {
  par(mfrow = c(2, 3))
  
  # Cube 1
  plot(density(df_numeric$xOff), col='blue', main="xOffset")
  plot(density(df_numeric$yOff), col='blue', main="yOffset")
  plot(density(df_numeric$dropOff), col='blue', main="DropOffset")
  plot(density(df_numeric$color_Cube_up), col='blue', main="Cube Color Up") # Cube that will be stacked 
  plot(density(df_numeric$color_Cube_down), col='blue', main="Cube Color Down") # Base Cube that the other cube is stacked on
}

# print some basic statistics
if (enable_print) {
  print("_____________")
  print("Stacking a Cube:")
  print(paste("Number of samples:", nrow(df_numeric)))
  print(paste("Number of failures: ", sum(df_numeric$Outcome==1), " (", sum(df_numeric$Outcome==1)/nrow(df_numeric), "%)", sep=""))
}

# discretize the data
breaks=c(7,5,5)
df_disc <- disc(df_numeric, breaks)
# learn causal BN
algo <- "gs" # using PC algorithm; other option is GS.
wl <- c()
g <- learnGraph(df_disc, algo, wl)

if (enable_print) {
  print("_____________")
  print("Variable Description:")
  print("xOff: distance between cube centers in x-direction")
  print("yOff: distance between cube centers in y-direction")
  print("dropOff: distance between cube surfaces in z-direction")
  print("colorDown: color of the bottom cube (red, blue, green or orange)")
  print("colorUp: color of the cube that is being stacked (red, blue, green or orange)")
  print("onTop: denotes if CubeUp is on top of CubeDown after the stacking process")
}

# learn failure prevention policy
epsilon <- 0.8
df_disc$onTop <- as.character(df_disc$onTop) #the outcome variables (in this case "inside") must be transformed into characters!
# generate failure explanation lookup table for all possible failures
failPol <- generateFailurePolicyCpp(df_disc, arcs(g@graph), "onTop", c(""), epsilon, enable_debug)


# Query the three examples from the paper (Table I)
# Example 1: xOff=0.0 - yOff=0.02 - dropOff=0.08
print("Diehl22 - Table I - CubeStacking - Example 1:")
print(failPol[failPol$xOff==unique(failPol$xOff)[3] & failPol$yOff==unique(failPol$yOff)[5] & failPol$dropOff==unique(failPol$dropOff)[6],])

# Example 2: xOff=0.015 - yOff=0.0 - dropOff=0.05
print("Diehl22 - Table I - CubeStacking - Example 2:")
print(failPol[failPol$xOff==unique(failPol$xOff)[4] & failPol$yOff==unique(failPol$yOff)[3] & failPol$dropOff==unique(failPol$dropOff)[4],])

# Example 3: xOff=-0.015 - yOff=0.015 - dropOff=0.02
print("Diehl22 - Table I - CubeStacking - Example 3:")
print(failPol[failPol$xOff==unique(failPol$xOff)[1] & failPol$yOff==unique(failPol$yOff)[1] & failPol$dropOff==unique(failPol$dropOff)[2],])





